<?php
namespace Controllers;

use \Core\Controller;
use Models\Order;
use Models\Orders;

class OrderController extends Controller {
    private $dataInfo;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->dataInfo[] = '';
        $this->render('order', $this->dataInfo);
    }

    public function create()
    {
        $orderModel = new Orders();

        $orderModel->create((new Order())
            ->setName(addslashes($_POST['name']))
            ->setPhone(addslashes($_POST['phone']))
            ->setDestinationCep(addslashes($_POST['destinationCep']))
            ->setDestinationCity(addslashes($_POST['destinationCity']))
            ->setDestinationStreet(addslashes($_POST['destinationStreet']))
            ->setDestinationNumber(addslashes($_POST['destinationNumber']))
            ->setDestinationNeighborhood(addslashes($_POST['destinationNeighborhood']))
            ->setOriginCep(addslashes($_POST['originCep']))
            ->setOriginCity(addslashes($_POST['originCity']))
            ->setOriginStreet(addslashes($_POST['originStreet']))
            ->setOriginNumber(addslashes($_POST['originNumber']))
            ->setOriginNeighborhood(addslashes($_POST['originNeighborhood']))
        );

        if ($orderModel) {
            header("Location: ".BASE_URL);
        }
    }

    public function update($orderId)
    {
        $orderModel = new Orders();

        $this->dataInfo['order'] = $orderModel->findOrderById($orderId)[0];

        $this->render('update', $this->dataInfo);
    }

    public function onUpdate($orderId)
    {
        $orderModel = new Orders();

        $orderEntity = (new Order())
            ->setId(addslashes($orderId))
            ->setName(addslashes($_POST['name']))
            ->setPhone(addslashes($_POST['phone']))
            ->setDestinationCep(addslashes($_POST['destinationCep']))
            ->setDestinationCity(addslashes($_POST['destinationCity']))
            ->setDestinationStreet(addslashes($_POST['destinationStreet']))
            ->setDestinationNumber(addslashes($_POST['destinationNumber']))
            ->setDestinationNeighborhood(addslashes($_POST['destinationNeighborhood']))
            ->setOriginCep(addslashes($_POST['originCep']))
            ->setOriginCity(addslashes($_POST['originCity']))
            ->setOriginStreet(addslashes($_POST['originStreet']))
            ->setOriginNumber(addslashes($_POST['originNumber']))
            ->setOriginNeighborhood(addslashes($_POST['originNeighborhood']));

        $orderModel->update($orderEntity);

        if ($orderModel) {
            header("Location: ". BASE_URL);
        }

    }

    public function delete($orderId)
    {
        $orderModel = new Orders();

        $orderModel->delete($orderId);

        if ($orderModel) {
            header("Location: ".BASE_URL);
        }
    }
}
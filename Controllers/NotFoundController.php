<?php
namespace Controllers;

use \Core\Controller;
use Models\Users;

class NotFoundController extends Controller {
    private $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [];
        $this->render('404', $data);
    }

}
<?php
namespace Controllers;

use \Core\Controller;
use Models\Orders;

class HomeController extends Controller {
	private $dataInfo;

	public function __construct()
    {
        parent::__construct();
	}

	public function index()
    {
        $orders = (new Orders())->find();

        $this->dataInfo['orders'] = $orders;

		$this->render('home', $this->dataInfo);
	}
}
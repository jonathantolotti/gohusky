<div class="row">
    <h2 class="text-center col-12">Dados do cliente:</h2>
</div>

<div class="row">
    <div class="container" id="sectionNewOrder">
        <form  method="post" action="<?php echo BASE_URL; ?>order/onUpdate/<?php echo $order->getId(); ?>">
            <div class="form-row">
                <div class="col-sm">
                    <label for="name" class="col-form-label">Nome:</label>
                    <input type="text" class="form-control" name="name" id="name" value="<?php echo $order->getName(); ?>" required>
                </div>
                <div class="col-sm">
                    <label for="phone" class="col-form-label">Telefone:</label>
                    <input type="text" class="form-control phone" name="phone" id="phone" value="<?php echo $order->getPhone(); ?>" required>
                </div>
            </div>
            <div class="row">
                <h2 class="text-center col-12">Dados de origem:</h2>
            </div>
            <div class="form-row">
                <div class="col-sm-3">
                    <label for="originCep" class="col-form-label">Cep:</label>
                    <input type="text" class="form-control orderCep" maxlength="9" name="originCep" value="<?php echo $order->getOriginCep(); ?>" id="originCep" required>
                </div>
                <div class="col-sm-2">
                    <label for="originCity" class="col-form-label">Cidade:</label>
                    <input type="text" class="form-control" name="originCity" value="<?php echo $order->getOriginCity(); ?>" id="originCity" required>
                </div>
                <div class="col-sm">
                    <label for="originStreet" class="col-form-label">Rua:</label>
                    <input type="text" class="form-control" name="originStreet" value="<?php echo $order->getOriginStreet(); ?>" id="originStreet" required>
                </div>
                <div class="col-sm-1">
                    <label for="originNumber" class="col-form-label">Número:</label>
                    <input type="text" class="form-control" name="originNumber" value="<?php echo $order->getOriginNumber(); ?>" id="originNumber" required>
                </div>
                <div class="col-sm-2">
                    <label for="originNeighborhood" class="col-form-label">Bairro:</label>
                    <input type="text" class="form-control" name="originNeighborhood" value="<?php echo $order->getOriginNeighborhood(); ?>" id="originNeighborhood" required>
                </div>
            </div>

            <div class="row">
                <h2 class="text-center col-12">Dados de destino:</>
            </div>
            <div class="form-row">
                <div class="col-sm-3">
                    <label for="destinationCep" class="col-form-label">Cep:</label>
                    <input type="text" class="form-control orderCep" name="destinationCep" value="<?php echo $order->getDestinationCep(); ?>" id="destinationCep" required>
                </div>
                <div class="col-sm-2">
                    <label for="destinationCity" class="col-form-label">Cidade:</label>
                    <input type="text" class="form-control" name="destinationCity" value="<?php echo $order->getDestinationCity(); ?>" id="destinationCity" required>
                </div>
                <div class="col-sm">
                    <label for="destinationStreet" class="col-form-label">Rua:</label>
                    <input type="text" class="form-control" name="destinationStreet" value="<?php echo $order->getDestinationStreet(); ?>" id="destinationStreet" required>
                </div>
                <div class="col-sm-1">
                    <label for="destinationNumber" class="col-form-label">Número:</label>
                    <input type="text" class="form-control" name="destinationNumber" value="<?php echo $order->getDestinationNumber(); ?>" id="destinationNumber" required>
                </div>
                <div class="col-sm-2">
                    <label for="destinationNeighborhood" class="col-form-label">Bairro:</label>
                    <input type="text" class="form-control" name="destinationNeighborhood" value="<?php echo $order->getDestinationNeighborhood(); ?>" id="destinationNeighborhood" required>
                </div>
            </div>

            <div class="row justify-content-center">
                <input class="btn btn-sm btn-success btnNewOrder" type="submit" value="Salvar">
                <input class="btn btn-sm btn-info btnNewOrder" type="reset" value="Limpar">
            </div>

        </form>
    </div>
</div>
<?php if (isset($_SESSION['alert'])): ?>
    <script src="<?php echo BASE_URL; ?>assets/js/sweetalert2.js"></script>
    <script>
        Swal.fire({
                title: '<?php echo $_SESSION['alert']['msgBody']; ?>',
                type: '<?php echo $_SESSION['alert']['type']; ?>',
                showConfirmButton: false,
                timer: 2000
        });

    </script>
<?php unset($_SESSION['alert']); ?>
<?php endif; ?>
<div class="container">
    <div class="row">
        <div class="row-10">
            <h1 class="col-sm-12 text-center">Listagem de pedidos:</h1>
        </div>
        <div class="col-sm">
            <a href="<?php echo BASE_URL;?>order" class="btn btn-sm btn-success float-right btnList">Novo pedido</a>
        </div>
    </div>

    <?php if (! $orders): ?>
        <div class="alert alert-dark text-center col-sm-12" role="alert">
            Você não tem nenhum pedido cadastrado! <a href="<?php echo BASE_URL;?>order" class="alert-link">Clique aqui</a> para cadastrar um pedido.
        </div>
    <?php else: ?>
    <div id="accordion">
        <?php foreach ($orders as $order): ?>
            <div class="card">
                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne-<?php echo $order->getId(); ?>">
                    <h5 class="mb-0 text-center">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne-<?php echo $order->getId(); ?>" aria-expanded="true" aria-controls="collapseOne">
                            <span class="text-center"><?php echo $order->getName(). ' - <span class="phone">'. $order->getPhone().'</span> - Pedido: '.$order->getId(); ?></span>
                        </button>
                    </h5>
                </div>

                <div id="collapseOne-<?php echo $order->getId(); ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <p class="text-center font-weight-bold text-primary">Dados de origem:</p>
                                <p class="text-center">
                                    <?php echo $order->getOriginStreet(); ?>,
                                    <?php echo $order->getOriginNumber(); ?> - <?php echo $order->getOriginNeighborhood(); ?> -
                                    <?php echo $order->getOriginCity(); ?> - <span class="orderCep"><?php echo $order->getOriginCep(); ?></span>
                                </p>
                            </div>
                            <div class="col-sm-5">
                                <p class="text-center font-weight-bold text-success">Dados de entrega:</p>
                                <p class="text-center">
                                    <?php echo $order->getDestinationStreet(); ?>,
                                    <?php echo $order->getDestinationNumber(); ?> - <?php echo $order->getDestinationNeighborhood(); ?> -
                                    <?php echo $order->getDestinationCity(); ?> - <span class="orderCep"><?php echo $order->getDestinationCep(); ?></span>
                            </div>
                            <div class="col-sm-2">
                                <p class="text-center font-weight-bold">Ações:</p>
                                <div class="text-center">
                                    <a href="<?php echo BASE_URL;?>order/update/<?php echo $order->getId(); ?>" class="action"><img src="<?php echo BASE_URL;?>assets/image/edit.png" class="img-fluid"> </a>
                                    <a href="<?php echo BASE_URL;?>order/delete/<?php echo $order->getId(); ?>" class="action"> <img src="<?php echo BASE_URL;?>assets/image/trash.png" class="img-fluid"> </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>

</div>
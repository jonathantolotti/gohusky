$(function () {
    $(".phone").mask('(00) 00000-0000');
    $(".orderCep").mask('00000-000');

    var deleted = false;

    $("#originCep").blur(function () {
        var originCep = $(this).val().replace(/\D/g, '');

        if (originCep.length == 0) {
            $("#originCity").val("");
            $("#originStreet").val("");
            $("#originNeighborhood").val("");
        }

        $.ajax({
            url: "https://viacep.com.br/ws/"+originCep+"/json",
            type:'GET',
            crossDomain: true,
            dataType: 'jsonp',
            success:function(data){
                $("#originCity").val(data.localidade);
                $("#originStreet").val(data.logradouro);
                $("#originNeighborhood").val(data.bairro);

                $("#originNumber").focus();

            }
        });

    });

    $("#destinationCep").blur(function () {
        var destinationCep = $(this).val().replace(/\D/g, '');

        if (destinationCep.length == 0) {
            $("#destinationCity").val("");
            $("#destinationStreet").val("");
            $("#destinationNeighborhood").val("");
        }

        $.ajax({
            url: "https://viacep.com.br/ws/"+destinationCep+"/json",
            type:'GET',
            crossDomain: true,
            dataType: 'jsonp',
            success:function(data){
                $("#destinationCity").val(data.localidade);
                $("#destinationStreet").val(data.logradouro);
                $("#destinationNeighborhood").val(data.bairro);

                $("#destinationNumber").focus();

            }
        });

    });

    function deleted() {
        Swal.fire(
            'Good job!',
            'You clicked the button!',
            'success'
        );
    }
    
});
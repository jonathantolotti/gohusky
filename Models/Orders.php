<?php

/*
 * Classe responsável por gerenciar a comunicação do banco de dados com a tabela de pedidos.
 *
 * @author Jonathan Tolotti <jonathanstolotti@gmail.com>
 */

namespace Models;

use Core\Model;
use PDO;

class Orders extends Model
{
    public function create(Order $order)
    {
        $sql = "INSERT INTO orders SET 
                name = :name,
                phone = :phone,
                origin_cep = :originCep,
                origin_city = :originCity,
                origin_street = :originStreet,
                origin_number = :originNumber,
                origin_neighborhood = :originNeighborhood,
                destination_cep = :destinationCep,
                destination_city = :destinationCity,
                destination_street = :destinationStreet,
                destination_number = :destinationNumber,
                destination_neighborhood = :destinationNeighborhood";

        $sql = $this->db->prepare($sql);

        $sql->bindValue(":name", $order->getName());
        $sql->bindValue(":phone", $order->getPhone());
        $sql->bindValue(":originCep", $order->getOriginCep());
        $sql->bindValue(":originCity", $order->getOriginCity());
        $sql->bindValue(":originStreet", $order->getOriginStreet());
        $sql->bindValue(":originNumber", $order->getOriginNumber());
        $sql->bindValue(":originNeighborhood", $order->getOriginNeighborhood());
        $sql->bindValue(":destinationCep", $order->getDestinationCep());
        $sql->bindValue(":destinationCity", $order->getDestinationCity());
        $sql->bindValue(":destinationStreet", $order->getDestinationStreet());
        $sql->bindValue(":destinationNumber", $order->getDestinationNumber());
        $sql->bindValue(":destinationNeighborhood", $order->getDestinationNeighborhood());

       try {
            $sql->execute();
           $_SESSION['alert'] = [
               'msgBody' => 'Pedido criado com sucesso.',
               'type' => 'success'
           ];
            return true;
       } catch (\PDOException $exception) {
           echo $exception->getMessage();
       }
       return false;
    }

    public function find()
    {
        $sql = "SELECT * FROM orders";

        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $orderEntity = [];

            $orders = $sql->fetchAll(PDO::FETCH_ASSOC);

            foreach ($orders as $order) {
                $orderEntity[] = ((new Order())
                    ->setId($order['id'])
                    ->setName($order['name'])
                    ->setPhone($order['phone'])
                    ->setDestinationCep($order['destination_cep'])
                    ->setDestinationCity($order['destination_city'])
                    ->setDestinationStreet($order['destination_street'])
                    ->setDestinationNumber($order['destination_number'])
                    ->setDestinationNeighborhood($order['destination_neighborhood'])
                    ->setOriginCep($order['origin_cep'])
                    ->setOriginCity($order['origin_city'])
                    ->setOriginStreet($order['origin_street'])
                    ->setOriginNumber($order['origin_number'])
                    ->setOriginNeighborhood($order['origin_neighborhood']));
            }

            return $orderEntity;
        }
        return false;
    }

    public function findOrderById($orderId)
    {
        $sql = "SELECT * FROM orders where id = {$orderId}";

        $sql = $this->db->query($sql);

        if ($sql->rowCount() > 0) {
            $orderEntity = [];

            $order = $sql->fetch(PDO::FETCH_ASSOC);

                $orderEntity[] = ((new Order())
                    ->setId($order['id'])
                    ->setName($order['name'])
                    ->setPhone($order['phone'])
                    ->setDestinationCep($order['destination_cep'])
                    ->setDestinationCity($order['destination_city'])
                    ->setDestinationStreet($order['destination_street'])
                    ->setDestinationNumber($order['destination_number'])
                    ->setDestinationNeighborhood($order['destination_neighborhood'])
                    ->setOriginCep($order['origin_cep'])
                    ->setOriginCity($order['origin_city'])
                    ->setOriginStreet($order['origin_street'])
                    ->setOriginNumber($order['origin_number'])
                    ->setOriginNeighborhood($order['origin_neighborhood']));

            return $orderEntity;
        }
        return false;
    }

    public function update(Order $order)
    {
        $sql = "UPDATE orders SET 
                name = :name,
                phone = :phone,
                origin_cep = :originCep,
                origin_city = :originCity,
                origin_street = :originStreet,
                origin_number = :originNumber,
                origin_neighborhood = :originNeighborhood,
                destination_cep = :destinationCep,
                destination_city = :destinationCity,
                destination_street = :destinationStreet,
                destination_number = :destinationNumber,
                destination_neighborhood = :destinationNeighborhood
                WHERE id = :id";

        $sql = $this->db->prepare($sql);

        $sql->bindValue(":id", $order->getId());
        $sql->bindValue(":name", $order->getName());
        $sql->bindValue(":phone", $order->getPhone());
        $sql->bindValue(":originCep", $order->getOriginCep());
        $sql->bindValue(":originCity", $order->getOriginCity());
        $sql->bindValue(":originStreet", $order->getOriginStreet());
        $sql->bindValue(":originNumber", $order->getOriginNumber());
        $sql->bindValue(":originNeighborhood", $order->getOriginNeighborhood());
        $sql->bindValue(":destinationCep", $order->getDestinationCep());
        $sql->bindValue(":destinationCity", $order->getDestinationCity());
        $sql->bindValue(":destinationStreet", $order->getDestinationStreet());
        $sql->bindValue(":destinationNumber", $order->getDestinationNumber());
        $sql->bindValue(":destinationNeighborhood", $order->getDestinationNeighborhood());

        try {
            $sql->execute();
            $_SESSION['alert'] = [
                'msgBody' => 'Pedido alterado com sucesso.',
                'type' => 'success'
            ];
            return true;
        } catch (\PDOException $exception) {
            echo $exception->getMessage();
        }
        return false;
    }

    public function delete($orderId)
    {
        $sql = "DELETE FROM orders WHERE id = {$orderId}";

        $sql = $this->db->query($sql);

        if ($sql) {
            $_SESSION['alert'] = [
                'msgBody' => 'Pedido deletado com sucesso.',
                'type' => 'success'
            ];
            return true;
        }

        return false;
    }
}
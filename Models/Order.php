<?php
/*
 * Entidade de pedidos.
 *
 * @author Jonathan Tolotti <jonathanstolotti@gmail.com> *
 */
namespace Models;


class Order
{
    /**  @var $id */
    private $id;

    /** @var $name */
    private $name;

    /** @var $phone */
    private $phone;

    /** @var $originCep */
    private $originCep;

    /** @var $originCity */
    private $originCity;

    /** @var $originStreet */
    private $originStreet;

    /** @var $originNumber */
    private $originNumber;

    /** @var $originNeighborhood */
    private $originNeighborhood;

    /** @var $destinationCep */
    private $destinationCep;

    /** @var $destinationCity */
    private $destinationCity;

    /** @var $destinationStreet */
    private $destinationStreet;

    /** @var $destinationNumber */
    private $destinationNumber;

    /** @var $destinationNeighborhood */
    private $destinationNeighborhood;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Order
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Order
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     * @return Order
     */
    public function setPhone($phone)
    {
        $this->phone = str_replace(['(', ')', '-', ' '],'',$phone);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginCep()
    {
        return $this->originCep;
    }

    /**
     * @param mixed $originCep
     * @return Order
     */
    public function setOriginCep($originCep)
    {
        $this->originCep = str_replace('-', '', $originCep);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginCity()
    {
        return $this->originCity;
    }

    /**
     * @param mixed $originCity
     * @return Order
     */
    public function setOriginCity($originCity)
    {
        $this->originCity = $originCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginStreet()
    {
        return $this->originStreet;
    }

    /**
     * @param mixed $originStreet
     * @return Order
     */
    public function setOriginStreet($originStreet)
    {
        $this->originStreet = $originStreet;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginNumber()
    {
        return $this->originNumber;
    }

    /**
     * @param mixed $originNumber
     * @return Order
     */
    public function setOriginNumber($originNumber)
    {
        $this->originNumber = $originNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOriginNeighborhood()
    {
        return $this->originNeighborhood;
    }

    /**
     * @param mixed $originNeighborhood
     * @return Order
     */
    public function setOriginNeighborhood($originNeighborhood)
    {
        $this->originNeighborhood = $originNeighborhood;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinationCep()
    {
        return $this->destinationCep;
    }

    /**
     * @param mixed $destinationCep
     * @return Order
     */
    public function setDestinationCep($destinationCep)
    {
        $this->destinationCep = str_replace('-', '', $destinationCep);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinationCity()
    {
        return $this->destinationCity;
    }

    /**
     * @param mixed $destinationCity
     * @return Order
     */
    public function setDestinationCity($destinationCity)
    {
        $this->destinationCity = $destinationCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinationStreet()
    {
        return $this->destinationStreet;
    }

    /**
     * @param mixed $destinationStreet
     * @return Order
     */
    public function setDestinationStreet($destinationStreet)
    {
        $this->destinationStreet = $destinationStreet;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinationNumber()
    {
        return $this->destinationNumber;
    }

    /**
     * @param mixed $destinationNumber
     * @return Order
     */
    public function setDestinationNumber($destinationNumber)
    {
        $this->destinationNumber = $destinationNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestinationNeighborhood()
    {
        return $this->destinationNeighborhood;
    }

    /**
     * @param mixed $destinationNeighborhood
     * @return Order
     */
    public function setDestinationNeighborhood($destinationNeighborhood)
    {
        $this->destinationNeighborhood = $destinationNeighborhood;
        return $this;
    }
}
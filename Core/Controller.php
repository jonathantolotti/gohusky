<?php

namespace Core;

class Controller {

	protected $db;

	/**
	 * Controller constructor.
	 */
	public function __construct() {
		global $config;
	}

	/**
	 * @param       $viewName
	 * @param array $viewData
	 */
	public function loadView($viewName, $viewData = []) {
		extract($viewData);
		include 'Views/' . $viewName . '.php';
	}

	/**
	 * @param       $viewName
	 * @param array $viewData
	 */
	public function render($viewName, $viewData = []) {
		include 'Views/template.php';
	}

	/**
	 * @param $viewName
	 * @param $viewData
	 */
	public function loadViewInTemplate($viewName, $viewData) {
		extract($viewData);
		include 'Views/' . $viewName . '.php';
	}

}